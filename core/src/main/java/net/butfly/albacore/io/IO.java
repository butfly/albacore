package net.butfly.albacore.io;

import net.butfly.albacore.base.Sizable;

public interface IO extends Sizable, Openable {}
