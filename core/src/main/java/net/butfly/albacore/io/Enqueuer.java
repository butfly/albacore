package net.butfly.albacore.io;

import java.util.stream.Stream;

@FunctionalInterface
public interface Enqueuer<V> {
	void enqueue(Stream<V> items);

	default void failed(Stream<V> fails) {}

	default void succeeded(long success) {}
}
